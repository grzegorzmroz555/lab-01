from __future__ import annotations
from numpy.typing import NDArray
import numpy as np
import math
from saport.simplex.model import Model

eps = 0.000000001
TableauIndex = np.intp | int # to make things easier for students...


class Tableau:
    """
        A class to represent a tableau to linear programming problem.

        Attributes
        ----------
        model : Model
            model corresponding to the tableau
        table : numpy.Array
            2d-array with the tableau

        Methods
        -------
        __init__(model: Model, table: NDArray[np.double]) -> Tableau:
            constructs a new tableau for the specified model and initial table
        extract_basis() -> list[int]
            returns list of indexes corresponding to the variables belonging to the basis
    """
    model: Model
    table: NDArray[np.double]

    def __init__(self, model: Model, table: NDArray[np.double]):
        self.model = model
        self.table = table.copy()

    def extract_basis(self) -> list[int]:
        rows_n, cols_n = self.table.shape
        basis = [-1 for _ in range(rows_n -1)]
        for c in range(cols_n - 1):
            column = self.table[:,c]
            belongs_to_basis = math.isclose(column.min(), 0.0, abs_tol = eps) \
                           and math.isclose(column.max(), 1.0, abs_tol = eps) \
                           and math.isclose(column.sum(), 1.0, abs_tol = eps)
            if belongs_to_basis:
                row = np.where(column == 1.0)[0][0]
                # [row-1] because we ignore the cost variable in the basis
                basis[row-1] = c
        return basis
    
    def __str__(self) -> str:
        def cell(x: str, w: int) -> str:
            return '{0: >{1}}'.format(x, w)

        cost_name = self.model.objective.name()
        basis = self.extract_basis()
        header = ["basis", cost_name] + [var.name for var in self.model.variables] + ["b"]
        longest_col = max([len(h) for h in header])

        rows = [[cost_name]] + [[self.model.variables[i].name] for i in basis]

        for (i,r) in enumerate(rows):
            cost_factor = 0.0 if i > 0 else 1.0
            r += ["{:.3f}".format(v) for v in [cost_factor] + list(self.table[i])]
            longest_col = max(longest_col, max([len(v) for v in r]))

        header = [cell(h, longest_col) for h in header]
        rows = [[cell(v, longest_col) for v in row] for row in rows]

        cell_sep = " | "

        result = cell_sep.join(header) + "\n"
        for row in rows:
            result += cell_sep.join(row) + "\n"
        return result