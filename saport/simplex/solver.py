from __future__ import annotations

from copy import deepcopy
import numpy as np

from saport.simplex.model import Model
from saport.simplex.tableau import Tableau
from saport.simplex.expressions.objective import Objective, ObjectiveType
from saport.simplex.expressions.constraint import Constraint, ConstraintType
from saport.simplex.expressions.expression import Expression, Atom, Variable


class Solver:
    """
        A class to represent a simplex solver.

        Attributes:
        ______
        _slacks: dict[Variable, Constraint]:
            contains mapping from slack variables to their corresponding constraints
        _surpluses: dict[Variable, Constraint]:
            contains mapping from surplus variables to their corresponding constraints

        Methods
        -------
        solve(model: Model) -> Tableau:
            augments the given model and returns corresponding tableau
    """
    _slacks: dict[Variable, Constraint]
    _surpluses: dict[Variable, Constraint]

    def solve(self, model: Model) -> Tableau:
        model.validate()
        normal_model = self._augment_model(model)
        tableau = self._basic_initial_tableau(normal_model)
        return tableau


    def _augment_model(self, original_model: Model) -> Model:
        """
            _augment_model(model: smodel.Model) -> smodel.Model:
                returns an augmented version of the given model
        """
        # We don't want to modify the original model, so we copy it
        model = deepcopy(original_model)
        # Wa want to have simplified expressions 
        # each variable should occur only once in every expression
        model.simplify()

        # Let's make sure the model wants to maximize the objective
        self._change_objective_to_max(model)
        self._change_constraints_bounds_to_nonnegative(model)
        self._add_slack_and_surplus_variables(model)
        return model

    def _change_objective_to_max(self, model: Model):
        # TODO:
        # - if the objective is minimizing, we have to "invert" it
        #   tip 0. Objective is stroe in model.objective
        #   tip 1. Objective has `type` attribute storing ObjectiveType (.MIN / .MAX) 
        #   tip 2. Objective class has an `invert` method just for this purpose
        raise NotImplementedError()

    def _change_constraints_bounds_to_nonnegative(self, model: Model):
        # TODO: 
        # all the bounds in the augmented model have to be positive or equal zero
        # - every constraint with a negative bound has to be "inverted"
        #   tip 0. Constraints are stored in model.constraints
        #   tip 1. Constraint class has an "invert" method just for this purpose
        raise NotImplementedError()

    def _add_slack_and_surplus_variables(self, model: Model):
        # empty dictionaries to store informations about slacks / surplus variables
        slacks: dict[Variable, Constraint] = dict()    
        surpluses: dict[Variable, Constraint] = dict() 

        # TODO:
        # we have to iterate through the constraints and:
        # - if constraint has type ConstraintType.EQ, we can ignore it  
        # - if constraint has type ConstraintType.GE:
        #   * we need to create a new surplus variable in the model
        #   * subtract this variable from the constraint's expression 
        #   * store variable in the `surpluses` dictionary — the new variable is the key, the constraint should be a value
        #   * change type of constraint to ConstraintType.EQ
        # - if constraint of type ConstraintType.LE needs a new slack variable, that should be added to its expression
        #   and stored in `slacks` dictionary
        # tip 0. constraints are stored in model.constraints
        # tip 1. constraint has properties `type`, `expression`, and unique `index`
        # tip 2. '-' and '+' operators are overloaded for the expression type, so you can literally add/subtract variables
        #        to the expression. You can also use in-place versions: "-=" and "+="
        # tip 3. every variable in model should have a unique name, for now let's assume names `s1`, `s2`, `s3`, ..., are fine
        raise NotImplementedError()
        
        # we remember info about the slack and surpluses, it will be useful in the next class
        self._slacks = slacks
        self._surpluses = surpluses

    def _basic_initial_tableau(self, model: Model) -> Tableau:
        # TODO:
        # replace the 'None' below with a numpy array, where
        # 1) first row consists of the inverted coefficients of the objective expression 
        #    plus 0.0 in the last column
        # 2) every other row consists of the coefficitients in the corresponding constraints, 
        #    don't forget to put the constraint bound in the last column
        # tips.
        # - to invert coefficients in an expression, one can multiply it by `-1``
        # - to get coefficients one can use the `expression.get_coefficients` method on `model.variales`, 
        #   it will return a list of floats
        # 
        # - one can easily create an array based on an existing list, e.g.
        #   * having list of lists: a = [[1,2,3], [4,5,6], [7,8,9]]
        #   * using `np.array(a)` will result in corresponding array:
        #       |1 2 3|
        #       |4 5 6|
        #       |7 8 9|
        table = None
        raise NotImplementedError()
        return Tableau(model, table)
    